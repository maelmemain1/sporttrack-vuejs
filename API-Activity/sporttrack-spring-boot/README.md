# TP4 - API REST avec OpenAPI

- MEMAIN Maël - 2A1

## Avancé du tp :

- L'API permet de réaliser les opérations suivantes :
  - Récupérer toutes les activités : GET /activities
  - Rechercher des activités par mot-clé : GET /activities/{keyword}
  - Ajouter une nouvelle activité : POST /activities
  - Générer un swagger
  - Valider les objets Activity et Data
  - Gérer les exceptions de validations

## Comment l'utiliser ?

- faire un **`mvn clean install`** dans sporttrack-model
- faire un **`mvn clean package spring-boot:run`** dans sporttrack-spring-boot
- Vous pouvez maintenant faire des requêtes cURL sur localhost et sur le port 8080

## Récupérer toutes les activités :

- La commande pour récupérer toutes les activités est la suivante :

```bash
curl -X GET "http://localhost:8080/activities/"
```

- Résultat :

```json
[{"date":"03/01/2023","description":"IUT RU","distance":770,"freqMin":98,"freqMax":103,"data":[{"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0},{"time":"13:00:05","cardioFrequency":100,"latitude":47.64687,"longitude":-2.778911,"altitude":18.0},{"time":"13:00:10","cardioFrequency":102,"latitude":47.6462,"longitude":-2.78022,"altitude":18.0},{"time":"13:00:15","cardioFrequency":103,"latitude":47.6465,"longitude":-2.78153,"altitude":19.0},{"time":"13:00:20","cardioFrequency":101,"latitude":47.6468,"longitude":-2.78284,"altitude":19.5},{"time":"13:00:25","cardioFrequency":99,"latitude":47.6471,"longitude":-2.78415,"altitude":20.0}]},{"date":"03/01/2023","description":"Course ? pied","distance":770,"freqMin":98,"freqMax":103,"data":[{"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0},{"time":"13:00:05","cardioFrequency":100,"latitude":47.64687,"longitude":-2.778911,"altitude":18.0},{"time":"13:00:10","cardioFrequency":102,"latitude":47.6462,"longitude":-2.78022,"altitude":18.0},{"time":"13:00:15","cardioFrequency":103,"latitude":47.6465,"longitude":-2.78153,"altitude":19.0},{"time":"13:00:20","cardioFrequency":101,"latitude":47.6468,"longitude":-2.78284,"altitude":19.5},{"time":"13:00:25","cardioFrequency":99,"latitude":47.6471,"longitude":-2.78415,"altitude":20.0}]},{"date":"03/01/2023","description":"V?lo","distance":770,"freqMin":98,"freqMax":103,"data":[{"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0}]},{"date":"03/01/2023","description":"Moto","distance":10,"freqMin":98,"freqMax":1,"data":[{"time":"13:00:00","cardioFrequency":120,"latitude":47.644794,"longitude":-2.776605,"altitude":200.0}]}]%
```

## Récupérer les activités selon un mot-clé :

- La commande pour récupérer les activités selon un mot-clé est la suivante :

```bash
curl -X GET "http://localhost:8080/activities/mot-clé"
```

- Exemple avec **`curl -X GET "http://localhost:8080/activities/IUT"`** :
- Résultat :

```json
[{"date":"03/01/2023","description":"IUT RU","distance":770,"freqMin":98,"freqMax":103,"data":[{"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0},{"time":"13:00:05","cardioFrequency":100,"latitude":47.64687,"longitude":-2.778911,"altitude":18.0},{"time":"13:00:10","cardioFrequency":102,"latitude":47.6462,"longitude":-2.78022,"altitude":18.0},{"time":"13:00:15","cardioFrequency":103,"latitude":47.6465,"longitude":-2.78153,"altitude":19.0},{"time":"13:00:20","cardioFrequency":101,"latitude":47.6468,"longitude":-2.78284,"altitude":19.5},{"time":"13:00:25","cardioFrequency":99,"latitude":47.6471,"longitude":-2.78415,"altitude":20.0}]}]%
```

## Ajouter une activité :

- La commande pour ajouter une activité est la suivante :

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{informations sur une activité au format json}'
```

- Exemple avec :

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{
        "date":"03/01/2023",
        "description":"Course au RU",
        "distance":770,
        "freqMin":98,
        "freqMax":103,
        "data":[
            {"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0},
            {"time":"13:00:05","cardioFrequency":100,"latitude":47.64687,"longitude":-2.778911,"altitude":18.0},
            {"time":"13:00:10","cardioFrequency":102,"latitude":47.6462,"longitude":-2.78022,"altitude":18.0},
            {"time":"13:00:15","cardioFrequency":103,"latitude":47.6465,"longitude":-2.78153,"altitude":19.0},
            {"time":"13:00:20","cardioFrequency":101,"latitude":47.6468,"longitude":-2.78284,"altitude":19.5},
            {"time":"13:00:25","cardioFrequency":99,"latitude":47.6471,"longitude":-2.78415,"altitude":20.0}
        ]
    }'
```

- Résultat :

```json
{"date":"03/01/2023","description":"Course au RU","distance":770,"freqMin":98,"freqMax":103,"data":[{"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0},{"time":"13:00:05","cardioFrequency":100,"latitude":47.64687,"longitude":-2.778911,"altitude":18.0},{"time":"13:00:10","cardioFrequency":102,"latitude":47.6462,"longitude":-2.78022,"altitude":18.0},{"time":"13:00:15","cardioFrequency":103,"latitude":47.6465,"longitude":-2.78153,"altitude":19.0},{"time":"13:00:20","cardioFrequency":101,"latitude":47.6468,"longitude":-2.78284,"altitude":19.5},{"time":"13:00:25","cardioFrequency":99,"latitude":47.6471,"longitude":-2.78415,"altitude":20.0}]}%
```

## Valider les objets Activity et Data :

- Pour tester la validation des objets Activity et Data il suffit d'ajouter une activité avec des données éronnées:
  - Valeurs nulles
  - Valeurs supérieures au maximum
  - Valeurs inféreieures au minimum
- Il peut y'avoir des erreurs pour l'une des valeurs ou bien plusieurs en même temps, les messages d'erreurs se mettrons à la suite.

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{informations sur une activité au format json}'
```

- Test avec des valeurs nulles :
- Exemple avec :

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{
        "date":"03/01/2023",
        "description":"Natation",
        "distance":120,
        "freqMin":50,
        "freqMax":180,
        "data":[]
    }'
```
- Résultat :

```json
{"data":"la taille doit être comprise entre 1 et 2147483647"}%  
```

- ou

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{
        "date":"03/01/2023",
        "description":"Natation",
        "distance":120,
        "freqMin":50,
        "freqMax":180,
        "data":[{}]
    }'
```

- Résultat :

```json
{"data[0].time":"ne doit pas être nul","data[0].cardioFrequency":"doit être supérieur ou égal à 1"}%
```

- ou

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{}'
```

- Résultat :

```json
{"date":"ne doit pas être nul","distance":"doit être supérieur ou égal à 1","data":"ne doit pas être nul","description":"ne doit pas être nul","freqMax":"doit être supérieur ou égal à 1","freqMin":"doit être supérieur ou égal à 1"}%
```

- Test avec des valeurs inférieures au minimum :
  - Par exemple avec une fréquence cardiaque négative, on peut le faire aussi avec toutes les valeurs (latitude, longitude et altitude).

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{informations sur une activité au format json}'
```

- Exemple avec :

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{
        "date":"03/01/2023",
        "description":"Course au RU",
        "distance":770,
        "freqMin":98,
        "freqMax":103,
        "data":[
            {"time":"13:00:00","cardioFrequency":-20,"latitude":-97.644794,"longitude":-92.776605,"altitude":-19000},
            {"time":"13:00:05","cardioFrequency":-20,"latitude":-97.64687,"longitude":-92.778911,"altitude":-19000},
            {"time":"13:00:10","cardioFrequency":-20,"latitude":-97.6462,"longitude":-92.78022,"altitude":-19000},
            {"time":"13:00:15","cardioFrequency":-20,"latitude":-97.6465,"longitude":-92.78153,"altitude":-20000},
            {"time":"13:00:20","cardioFrequency":-20,"latitude":-97.6468,"longitude":-92.78284,"altitude":-20000},
            {"time":"13:00:25","cardioFrequency":-20,"latitude":-97.6471,"longitude":-92.78415,"altitude":-20000}
        ]
    }'
```

- Résultat :

```json
{"data[3].altitude":"doit être supérieur ou égal à -11000","data[3].latitude":"doit être supérieur ou égal à -90","data[4].latitude":"doit être supérieur ou égal à -90","data[2].altitude":"doit être supérieur ou égal à -11000","data[4].cardioFrequency":"doit être supérieur ou égal à 1","data[5].latitude":"doit être supérieur ou égal à -90","data[2].latitude":"doit être supérieur ou égal à -90","data[2].cardioFrequency":"doit être supérieur ou égal à 1","data[5].altitude":"doit être supérieur ou égal à -11000","data[1].latitude":"doit être supérieur ou égal à -90","data[4].altitude":"doit être supérieur ou égal à -11000","data[0].latitude":"doit être supérieur ou égal à -90","data[1].cardioFrequency":"doit être supérieur ou égal à 1","data[0].altitude":"doit être supérieur ou égal à -11000","data[3].cardioFrequency":"doit être supérieur ou égal à 1","data[5].cardioFrequency":"doit être supérieur ou égal à 1","data[0].cardioFrequency":"doit être supérieur ou égal à 1","data[1].altitude":"doit être supérieur ou égal à -11000"}%
```

- Test avec des valeurs supérieures au maximum :
  - Par exemple avec une fréquence cardiaque supérieure à la fréquence cardiaque maximale, on peut le faire aussi avec toutes les valeurs (latitude, longitude et altitude).

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{informations sur une activité au format json}'
```

- Exemple avec :

```bash
curl -X POST http://localhost:8080/activities/ \
-H "Content-Type: application/json" \
-H "Accept: application/json" \
-d '{
        "date":"03/01/2023",
        "description":"Natation",
        "distance":1,
        "freqMin":1,
        "freqMax":1,
        "data":[
            {"time":"13:00:00","cardioFrequency":301,"latitude":97.644,"longitude":2000,"altitude":19000},
            {"time":"13:00:05","cardioFrequency":301,"latitude":97.644,"longitude":2000,"altitude":19000},
            {"time":"13:00:10","cardioFrequency":301,"latitude":97.644,"longitude":2000,"altitude":19000},
            {"time":"13:00:15","cardioFrequency":301,"latitude":97.644,"longitude":2000,"altitude":19000},
            {"time":"13:00:20","cardioFrequency":301,"latitude":97.644,"longitude":2000,"altitude":19000},
            {"time":"13:00:25","cardioFrequency":301,"latitude":97.645,"longitude":2000,"altitude":19000}
        ]
    }'
```

- Résultat :

```json
{"data[3].altitude":"doit être inférieur ou égal à 8848","data[4].latitude":"doit être inférieur ou égal à 90","data[3].latitude":"doit être inférieur ou égal à 90","data[2].altitude":"doit être inférieur ou égal à 8848","data[4].cardioFrequency":"doit être inférieur ou égal à 300","data[5].latitude":"doit être inférieur ou égal à 90","data[2].cardioFrequency":"doit être inférieur ou égal à 300","data[2].latitude":"doit être inférieur ou égal à 90","data[1].latitude":"doit être inférieur ou égal à 90","data[5].altitude":"doit être inférieur ou égal à 8848","data[4].altitude":"doit être inférieur ou égal à 8848","data[4].longitude":"doit être inférieur ou égal à 180","data[2].longitude":"doit être inférieur ou égal à 180","data[0].longitude":"doit être inférieur ou égal à 180","data[0].latitude":"doit être inférieur ou égal à 90","data[1].cardioFrequency":"doit être inférieur ou égal à 300","data[0].altitude":"doit être inférieur ou égal à 8848","data[3].cardioFrequency":"doit être inférieur ou égal à 300","data[5].cardioFrequency":"doit être inférieur ou égal à 300","data[0].cardioFrequency":"doit être inférieur ou égal à 300","data[5].longitude":"doit être inférieur ou égal à 180","data[1].longitude":"doit être inférieur ou égal à 180","data[1].altitude":"doit être inférieur ou égal à 8848","data[3].longitude":"doit être inférieur ou égal à 180"}%
```
