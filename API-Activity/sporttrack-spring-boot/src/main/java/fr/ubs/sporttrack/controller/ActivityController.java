package fr.ubs.sporttrack.controller;

import fr.ubs.sporttrack.model.Activity;
import fr.ubs.sporttrack.service.ActivityService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/activities")
public class ActivityController {

	private final ActivityService activityService;

	public ActivityController(ActivityService activityService) {
		this.activityService = activityService;
	}

	@Operation(summary = "Find all activities", description = "Retrieve a list of all activities available in the system.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully retrieved list", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Activity.class))}),
			@ApiResponse(responseCode = "500", description = "Internal server error")
	})
	@GetMapping("/")
	public ResponseEntity<List<Activity>> findAll() {
		return ResponseEntity.ok(activityService.findAll());
	}

	@Operation(summary = "Find activities by keyword", description = "Retrieve a list of activities matching a specific keyword.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully retrieved list", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Activity.class))}),
			@ApiResponse(responseCode = "500", description = "Internal server error")
	})
	@GetMapping("/{keyword}")
	public ResponseEntity<List<Activity>> findByKeyword(
			@PathVariable String keyword) {
		return ResponseEntity.ok(activityService.findByKeyword(keyword));
	}

	@Operation(summary = "Add a new activity", description = "Add a new activity to the system. The new activity's data should be provided in the request body.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Activity created successfully", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class))}),
			@ApiResponse(responseCode = "400", description = "Invalid activity data provided"),
			@ApiResponse(responseCode = "500", description = "Internal server error")
	})
	@PostMapping(path = "/", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Activity> addActivity(
			@RequestBody @Valid Activity activity) {
		Activity createdActivity = activityService.addActivity(activity);
		return ResponseEntity.status(HttpStatus.CREATED).body(createdActivity);
	}
}
