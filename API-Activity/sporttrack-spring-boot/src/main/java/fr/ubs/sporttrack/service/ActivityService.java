package fr.ubs.sporttrack.service;

import fr.ubs.sporttrack.model.Activity;
import fr.ubs.sporttrack.model.JSONFileReader;
import fr.ubs.sporttrack.model.JSONFileWriter;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ActivityService {

	public List<Activity> findAll() {
		JSONFileReader fileReader;

		try {
			fileReader = new JSONFileReader(new File("src/main/resources/data.json"));
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}

		return fileReader.getActivities();
	}

	public List<Activity> findByKeyword(String keyword) {
		List<Activity> result = new ArrayList<>();

		for (Activity activity : findAll()) {
			if (activity.getDescription().toLowerCase().contains(keyword.toLowerCase())) {
				result.add(activity);
			}
		}

		return result;
	}

	public Activity addActivity(Activity activity){
		JSONFileWriter fileWriter = null;
		try {

			List<Activity> activities = findAll();
			activities.add(activity);

			fileWriter = new JSONFileWriter(new File("src/main/resources/data.json"));
			fileWriter.writeData(activities);

			fileWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return activity;
	}

}
