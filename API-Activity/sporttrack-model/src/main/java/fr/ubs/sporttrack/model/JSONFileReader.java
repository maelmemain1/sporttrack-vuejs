package fr.ubs.sporttrack.model;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class JSONFileReader {
    private List<Activity> activities;

    /**
     * Creates a JSONFileReader object that opens and reads the file
     * specified as parameter, and stores the content in a list of
     * objects of type Activity.
     * @param file The file to be read.
     * @exception IOException if the file does not exist, cannot be read, or the JSON is invalid.
     */
    public JSONFileReader(File file) throws IOException {
        this.activities = new ArrayList<>();

        if (!file.exists()) {
            throw new FileNotFoundException("File not found: " + file.getAbsolutePath());
        }

        StringBuilder jsonText = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                jsonText.append(line);
            }
        } catch (IOException e) {
            throw new IOException("Error reading file: " + file.getAbsolutePath(), e);
        }

        parseActivities(jsonText.toString(), file.getAbsolutePath());
    }

    private void parseActivities(String jsonData, String filePath) throws IOException {
        try {
            JSONArray activitiesArray = new JSONArray(jsonData);
            for (int i = 0; i < activitiesArray.length(); i++) {
                JSONObject activityJson = activitiesArray.getJSONObject(i);
                Activity activity = Activity.fromJSON(activityJson);
                activities.add(activity);
            }
        } catch (Exception e) {
            throw new IOException("Error parsing JSON from file: " + filePath, e);
        }
    }

    /**
     * Returns a list of Activity objects that have been read from the file.
     * @return a list of Activity objects.
     */
    public List<Activity> getActivities() {
        return this.activities;
    }
}
