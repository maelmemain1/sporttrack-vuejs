package fr.ubs.sporttrack.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.json.JSONObject;

public class Data {
    private static final String TIME_FIELD = "time";
    private static final String CARDIO_FREQ_FIELD = "cardioFrequency";
    private static final String LATITUDE_FIELD = "latitude";
    private static final String LONGITUDE_FIELD = "longitude";
    private static final String ALTITUDE_FIELD = "altitude";

    @NotNull
    private String time;

    @NotNull
    @Min(1)
    @Max(300)
    private int cardioFrequency;

    @NotNull
    @Min(-90)
    @Max(90)
    private float latitude;

    @NotNull
    @Min(-180)
    @Max(180)
    private float longitude;

    @NotNull
    @Min(-11000)
    @Max(8848)
    private float altitude;

    public Data(String time, int cf, float lat, float lon, float alt) {
        this.time = time;
        this.cardioFrequency = cf;
        this.latitude = lat;
        this.longitude = lon;
        this.altitude = alt;
    }

    public Data() {
    }

    public String getTime() {
        return this.time;
    }

    public int getCardioFrequency() {
        return this.cardioFrequency;
    }

    public float getLatitude() {
        return this.latitude;
    }

    public float getLongitude() {
        return this.longitude;
    }

    public float getAltitude() {
        return this.altitude;
    }

    public static Data fromJSON(JSONObject obj) {
        return new Data(obj.getString(TIME_FIELD), obj.getInt(CARDIO_FREQ_FIELD), obj.getFloat(LATITUDE_FIELD),
                obj.getFloat(LONGITUDE_FIELD), obj.getFloat(ALTITUDE_FIELD));
    }

    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put(TIME_FIELD, this.time);
        obj.put(CARDIO_FREQ_FIELD, this.cardioFrequency);
        obj.put(LATITUDE_FIELD, this.latitude);
        obj.put(LONGITUDE_FIELD, this.longitude);
        obj.put(ALTITUDE_FIELD, this.altitude);
        return obj;
    }

    @Override
    public String toString() {
        return this.toJSON().toString();
    }
}
