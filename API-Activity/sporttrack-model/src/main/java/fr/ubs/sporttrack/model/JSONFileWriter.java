package fr.ubs.sporttrack.model;

import org.json.JSONArray;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

public class JSONFileWriter{
    
    private BufferedWriter writer;

    /**
     * Creates a JSONFileWriter object that opens the file specified
     * as parameter in order to write objects of type
     * <code>Activity</code> into it.
     * @param f The file that must be opened.
     * @exception IOException if the file cannot be open in write mode.
     */
    public JSONFileWriter(File f) throws IOException{
        this.writer = new BufferedWriter(new FileWriter(f));
    }


    /**
     * Writes a list of objects of type <code>Activity</code> into
     * the file.
     * @param activities The activities that must be written into the file.
     * @exception IOException if an error occurs while writting data.
     */
    public void writeData(List<Activity> activities) throws IOException{
        JSONArray jsonArray = new JSONArray();
        for (Activity activity : activities) {
            jsonArray.put(activity.toJSON());
        }
        writer.write(jsonArray.toString(4));
    }


    /**
     * Closes the file.
     * @exception IOException if an error occurs while closing the file.
     */
    public void close() throws IOException{
        if (writer != null) {
            writer.close();
        }
    }

}
