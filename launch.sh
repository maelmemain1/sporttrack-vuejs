#!/bin/bash
#lancement api activités
osascript -e 'tell app "Terminal"
    do script "cd /Users/marlo/Documents/IUT/BUT2/S4/R4.01/tp5 && cd API-Activity/sporttrack-model; mvn clean install; cd ../sporttrack-spring-boot; mvn clean install; mvn clean package spring-boot:run"
end tell'

#lancement app  vue
osascript -e 'tell app "Terminal"
    do script "cd /Users/marlo/Documents/IUT/BUT2/S4/R4.01/tp5/vue-app && npm run serve"
end tell'

#lancement api users
cd API-User/sporttrack-model
mvn clean install
cd ../sporttrack-spring-boot
mvn clean install
mvn clean package spring-boot:run



