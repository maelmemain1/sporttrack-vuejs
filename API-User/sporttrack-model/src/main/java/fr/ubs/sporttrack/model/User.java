package fr.ubs.sporttrack.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.json.JSONObject;
import org.json.JSONArray;

import java.util.List;
import java.util.ArrayList;

public class User {
    private static final String USERNAME_FIELD = "username";
    private static final String PASSWORD_FIELD = "password";
    private static final String EMAIL_FIELD = "email";

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String email;

    public User() {
    }

    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getEmail() {
        return this.email;
    }

    public static User fromJSON(JSONObject obj) {
        return new User(obj.getString(USERNAME_FIELD), obj.getString(PASSWORD_FIELD), obj.getString(EMAIL_FIELD));
    }

    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put(USERNAME_FIELD, this.username);
        obj.put(PASSWORD_FIELD, this.password);
        obj.put(EMAIL_FIELD, this.email);
        return obj;
    }

    @Override
    public String toString() {
        return this.toJSON().toString();
    }
}
