package fr.ubs.sporttrack.model;

import org.json.JSONArray;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JSONFileWriter {
    
    private BufferedWriter writer;

    public JSONFileWriter(File f) throws IOException{
        this.writer = new BufferedWriter(new FileWriter(f));
    }

    public void writeData(List<User> users) throws IOException{
        JSONArray jsonArray = new JSONArray();
        for (User user : users) {
            jsonArray.put(user.toJSON());
        }
        writer.write(jsonArray.toString(4));
    }

    public void close() throws IOException{
        if (writer != null) {
            writer.close();
        }
    }
}