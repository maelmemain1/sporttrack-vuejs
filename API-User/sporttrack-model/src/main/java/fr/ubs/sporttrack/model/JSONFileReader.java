package fr.ubs.sporttrack.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class JSONFileReader {
    private List<User> users;

    public JSONFileReader(File file) throws IOException {
        this.users = new ArrayList<>();

        if (!file.exists()) {
            throw new FileNotFoundException("File not found: " + file.getAbsolutePath());
        }

        StringBuilder jsonText = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                jsonText.append(line);
            }
        } catch (IOException e) {
            throw new IOException("Error reading file: " + file.getAbsolutePath(), e);
        }

        parseUsers(jsonText.toString(), file.getAbsolutePath());
    }

    private void parseUsers(String jsonData, String filePath) throws IOException {
        try {
            JSONArray usersArray = new JSONArray(jsonData);
            for (int i = 0; i < usersArray.length(); i++) {
                JSONObject userJson = usersArray.getJSONObject(i);
                User user = User.fromJSON(userJson);
                users.add(user);
            }
        } catch (Exception e) {
            throw new IOException("Error parsing JSON from file: " + filePath, e);
        }
    }

    public List<User> getUsers() {
        return this.users;
    }
}
