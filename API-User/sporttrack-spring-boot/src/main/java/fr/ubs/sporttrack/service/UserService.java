package fr.ubs.sporttrack.service;

import fr.ubs.sporttrack.model.JSONFileReader;
import fr.ubs.sporttrack.model.JSONFileWriter;
import fr.ubs.sporttrack.model.User;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    public List<User> findAll() {
        JSONFileReader fileReader;

        try {
            fileReader = new JSONFileReader(new File("src/main/resources/data.json"));
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }

        return fileReader.getUsers();
    }

    public User findByUsername(String username) {
        for (User user : findAll()) {
            if (user.getUsername().equalsIgnoreCase(username)) {
                return user;
            }
        }
        return null;
    }

    public boolean findByMailAndPasswd(String mail, String passwd) {
        for (User user : findAll()) {
            if (user.getEmail().equalsIgnoreCase(mail) && user.getPassword().equals(passwd)) {
                return true;
            }
        }
        return false;
    }

    public User addUser(User user) {
        if (findByUsername(user.getUsername()) != null) {
            return null; // Username already exists
        }

        if (findByEmail(user.getEmail()) != null) {
            return null; // Email already exists
        }

        try {
            List<User> users = findAll();
            users.add(user);

            JSONFileWriter fileWriter = new JSONFileWriter(new File("src/main/resources/data.json"));
            fileWriter.writeData(users);
            fileWriter.close();

            return user;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public User logUser(User user) {
        for (User existingUser : findAll()) {
            if (existingUser.getEmail().equalsIgnoreCase(user.getEmail()) &&
                    existingUser.getPassword().equals(user.getPassword())) {
                return existingUser;
            }
        }
        return null;
    }

    private User findByEmail(String email) {
        for (User user : findAll()) {
            if (user.getEmail().equalsIgnoreCase(email)) {
                return user;
            }
        }
        return null;
    }
}
