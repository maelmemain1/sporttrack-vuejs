# TP5 - Micro-services

- MEMAIN Maël - 2A1 - 13/06/2024

## Avancé du tp :

- L'API permet de réaliser les opérations suivantes :
  - Récupérer tous les users : GET /users
  - Rechercher des users par mot-clé : GET /activities/{keyword}
  - Ajouter un nouvel users : POST /users


## Comment l'utiliser ?

- faire un **`mvn clean install`** dans sporttrack-model
- faire un **`mvn clean package spring-boot:run`** dans sporttrack-spring-boot
- Vous pouvez maintenant faire des requêtes cURL sur localhost et sur le port 8082

## Récupérer tous les users :

- La commande pour récupérer tous les users est la suivante :

```bash
curl -X GET "http://localhost:8082/users/"
```

- Résultat :

```json
[{"username":"marlo","password":"pwd1","email":"marlo@gmail.com"},{"username":"Lily","password":"pwd2","email":"lily@gmail.com"},{"username":"Evy","password":"pwd3","email":"evy@gmail.com"}]
```

## Récupérer les users selon un username :

- La commande pour récupérer les users selon un username est la suivante :

```bash
curl -X GET "http://localhost:8082/users/username"
```

- Exemple avec **`curl -X GET "http://localhost:8082/users/marlo"`** :
- Résultat :

```json
{"username":"marlo","password":"pwd1","email":"marlo@gmail.com"}
```

## Ajouter un user :

- La commande pour ajouter un nouvel utilisateur est la suivante :

```bash
curl -X POST \
  http://localhost:8082/users/ \
  -H 'Content-Type: application/json' \
  -d '{
    "username": "username",
    "password": "motDePasse",
    "email": "example@example.com"
}'
```

- Exemple avec :

```bash
curl -X POST \
  http://localhost:8082/users/ \
  -H 'Content-Type: application/json' \
  -d '{
    "username": "NewUser",
    "password": "mdp1",
    "email": "user@user.fr"
}'
```

- Résultat :

```json
[{"username":"marlo","password":"pwd1","email":"marlo@gmail.com"},{"username":"Lily","password":"pwd2","email":"lily@gmail.com"},{"username":"Evy","password":"pwd3","email":"evy@gmail.com"},{"username":"NewUser","password":"mdp1","email":"user@user.fr"}]
```
