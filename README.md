# TP5 - Micro-services MEMAIN Maël

- MEMAIN Maël - 2A1

## Avancé du tp :

- L'app vuejs permet de : 
    - Se créer un compte à l'aide d'un username d'une adresse mail et d'un mot de passe.
    - Se connecter à son compte en rentrant son username, son adresse mail et son mot de passe.
    - Visualiser la liste des activité lorsqu'on est connecté.

## Comment l'utiliser ?

- API-User
    - faire un **`mvn clean install`** dans sporttrack-model
    - faire un **`mvn clean package spring-boot:run`** dans sporttrack-spring-boot
    - L'api est maintenant disponible à l'adresse : http://localhost:8082/users/
- API-Activity
    - faire un **`mvn clean install`** dans sporttrack-model
    - faire un **`mvn clean package spring-boot:run`** dans sporttrack-spring-boot
    - L'api est maintenant disponible à l'adresse : http://localhost:8081/activities/
- Application Vue Sporttrack
    - Faire un **`npm i`** dans le repertoire **vue-app**
    - Ensuite, faire un **`npm run serve`**.
    - L'application est maintenant disponible à l'adresse : http://localhost:8080/
    - Il faut maintenant se créer un compte ou bien se connecter à son compte, ensuite on peut visualiser la liste des utilisateurs et la liste des activités.
    - Lorsque l'on veux se créer un compte et qu'il n'existe pas déjà, le compte est créé et la connexion à ce compte se fait automatiquement alors on peut directement visualiser la liste des activités.
    - Lorsque l'on veux se connecter, on rentre ses identifiants, si ils sont correctes, alors on est connecté et on peut visualiser la liste des activités.


## Compte existants : 
- Vous pouvez récupérer la liste des comptes existants via 'http://localhost:8082/users/'
- Voici la liste des comptes existants : 
    - [
    - {
        "username": "marlo",
        "password": "pwd1",
        "email": "marlo@gmail.com"
    },
    - {
        "username": "Lily",
        "password": "pwd2",
        "email": "lily@gmail.com"
    },
    - {
        "username": "Evy",
        "password": "pwd3",
        "email": "evy@gmail.com"
    },
    {
        "username": "NewUser",
        "password": "mdp1",
        "email": "user@user.fr"
    },
    - {
        "username": "malo",
        "password": "Malo",
        "email": "malo@gmail.com"
    },
    - {
        "username": "Ewen",
        "password": "Ewen",
        "email": "ewen@gmail.com"
    },
    - {
        "username": "newuserrr",
        "password": "user",
        "email": "user@user.com"
    }
    ]


## Activités existantes : 
- Vous pouvez récupérer la liste des activités existantes via 'http://localhost:8081/activities/'
- Voici la liste des activités existantes : 
    - [{"date":"03/01/2023","description":"IUT RU","distance":770,"freqMin":98,"freqMax":103,"data":[{"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0},{"time":"13:00:05","cardioFrequency":100,"latitude":47.64687,"longitude":-2.778911,"altitude":18.0},{"time":"13:00:10","cardioFrequency":102,"latitude":47.6462,"longitude":-2.78022,"altitude":18.0},{"time":"13:00:15","cardioFrequency":103,"latitude":47.6465,"longitude":-2.78153,"altitude":19.0},{"time":"13:00:20","cardioFrequency":101,"latitude":47.6468,"longitude":-2.78284,"altitude":19.5},{"time":"13:00:25","cardioFrequency":99,"latitude":47.6471,"longitude":-2.78415,"altitude":20.0}]},

    - {"date":"03/01/2023","description":"Course ? pied","distance":770,"freqMin":98,"freqMax":103,"data":[{"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0},{"time":"13:00:05","cardioFrequency":100,"latitude":47.64687,"longitude":-2.778911,"altitude":18.0},{"time":"13:00:10","cardioFrequency":102,"latitude":47.6462,"longitude":-2.78022,"altitude":18.0},{"time":"13:00:15","cardioFrequency":103,"latitude":47.6465,"longitude":-2.78153,"altitude":19.0},{"time":"13:00:20","cardioFrequency":101,"latitude":47.6468,"longitude":-2.78284,"altitude":19.5},{"time":"13:00:25","cardioFrequency":99,"latitude":47.6471,"longitude":-2.78415,"altitude":20.0}]},

    - {"date":"03/01/2023","description":"V?lo","distance":770,"freqMin":98,"freqMax":103,"data":[{"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0}]},

    - {"date":"03/01/2023","description":"Moto","distance":10,"freqMin":98,"freqMax":1,"data":[{"time":"13:00:00","cardioFrequency":120,"latitude":47.644794,"longitude":-2.776605,"altitude":200.0}]},

    - {"date":"03/01/2023","description":"Course au RU","distance":770,"freqMin":98,"freqMax":103,"data":[{"time":"13:00:00","cardioFrequency":99,"latitude":47.644794,"longitude":-2.776605,"altitude":18.0},{"time":"13:00:05","cardioFrequency":100,"latitude":47.64687,"longitude":-2.778911,"altitude":18.0},{"time":"13:00:10","cardioFrequency":102,"latitude":47.6462,"longitude":-2.78022,"altitude":18.0},{"time":"13:00:15","cardioFrequency":103,"latitude":47.6465,"longitude":-2.78153,"altitude":19.0},{"time":"13:00:20","cardioFrequency":101,"latitude":47.6468,"longitude":-2.78284,"altitude":19.5},{"time":"13:00:25","cardioFrequency":99,"latitude":47.6471,"longitude":-2.78415,"altitude":20.0}]}]